import importlib
from unittest import TestCase


class TestBoilerplate(TestCase):

    def test_inkex_can_be_imported(self) -> None:
        try:
            importlib.import_module('inkex')
        except Exception as e:
            self.fail(
                "Importing inkex raised an Exception unexpectedly : %s" % e
            )
