PACKAGE_NAME=inkscape-plugin-boilerplate


test:
	clear && pipenv run python -m unittest

watch-test:
	watchmedo shell-command --patterns='*.py' \
							--drop \
							--recursive \
							--command='make test'

lint:
	python -m flake8 $(PACKAGE_NAME) tests

watch-lint:
	watchmedo shell-command --patterns='*.py' \
							--drop \
							--recursive \
							--command='make lint'

isort:
	isort --apply --recursive $(PACKAGE_NAME) tests


lib/inkscape-core-extensions:
	git clone https://gitlab.com/inkscape/extensions.git lib/inkscape-core-extensions


develop: lib/inkscape-core-extensions
	pipenv install --dev
