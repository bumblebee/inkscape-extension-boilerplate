# inkscape-extention-boilerplate

Quick boiler plate to develop inkscape extensions compatibles with  1.0 branch,
including tests.

Its use pipenv to manage libraires, to install it :
```
sudo pip installl pipenv
```

The Makefile provides some target to help developpement :

- `make develop` : install developpement envirement (virtualenv,
   dependecies... )
- `make test` : launch unit tests
- `watch-test` : monitor file modifications and auto-launch unit tests
- `make lint` : launch flake8 linter
- `watch-lint` : monitor file modifications and auto-launch linter
- `make isort` : clean import definitions
