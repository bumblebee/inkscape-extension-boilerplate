#!/usr/bin/env python3

from setuptools import setup
from setuptools import find_packages


setup(
    name='inkcape-extention-boilerplate',
    version='0.0.1',
    packages=find_packages(),
    setup_requires=[],
    install_requires=[
        'lxml'
    ]
)
